package ru.nlmk.study.jse70.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import ru.nlmk.study.jse70.model.Book;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("/api/book")
public class BookController {

    @ApiOperation("Получение книги по Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Книга найдена"),
            @ApiResponse(code = 400, message = "Неверный id")
    })
    @GetMapping("/{id}")
    public Book findById(
            @ApiParam(name = "id", value = "id книги, которую мы ищем")
            @PathVariable
                    long id) {
        return new Book(1L, "Adventures of Robinzon", "Daniel Defo");
    }

    @ApiOperation("Получение полного списка книг")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Список получен")
    })
    @GetMapping("/")
    public Collection<Book> getAllBooks() {
        ArrayList<Book> books = new ArrayList<>();
        books.add(new Book(1L, "Adventures of Robinzon", "Daniel Defo"));
        books.add(new Book(2L, "Around the world in 80 days", "Jules Vern"));
        return books;
    }

    @ApiOperation("Получение книг на странице")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Список получен"),
            @ApiResponse(code = 400, message = "Неверный фильтр")
    })
    @GetMapping("/filter")
    public Page<Book> filterBooks(Pageable pageable) {
        return Page.empty();
    }

    @ApiOperation("Изменение книги")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Книга изменена"),
            @ApiResponse(code = 400, message = "Неверный запрос")
    })
    @PatchMapping("/{id}")
    public Book updateBook(@PathVariable("id") final String id, @NotNull @Valid @RequestBody final Book book) {
        return book;
    }


    @ApiOperation("Создание книги")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Книга создана"),
            @ApiResponse(code = 400, message = "Неверный запрос")
    })
    @PostMapping("/")
    public Book postBook(@RequestBody @NotNull @Valid final Book book) {
        return book;
    }

    @ApiOperation("Удаление книги по Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Книга удалена"),
            @ApiResponse(code = 400, message = "Неверный id")
    })
    @DeleteMapping("/{id}")
    public long deleteBook(@PathVariable final long id) {
        return id;
    }

    @ApiOperation("Добавление автора")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Автор добавлен"),
            @ApiResponse(code = 400, message = "Неверные данные")
    })
    @PostMapping("/author")
    public void createAuthor(@RequestBody String author) {
        System.out.println("Author created: " + author);
    }
}
