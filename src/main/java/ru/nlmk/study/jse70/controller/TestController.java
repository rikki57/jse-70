package ru.nlmk.study.jse70.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @ApiOperation("Тест для получения информации методом Get")
    @GetMapping("/test")
    public String getData() {
        return "Returning test data";
    }

    @ApiOperation("Тест для отправки информации методом Post")
    @PostMapping("/test")
    public String postData(String data) {
        return "received data " + data;
    }
}
