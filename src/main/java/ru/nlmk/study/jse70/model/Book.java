package ru.nlmk.study.jse70.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    private long id;

    @NotBlank
    @Size(min = 0, max = 100)
    private String title;

    @NotBlank
    @Size(min = 0, max = 50)
    private String author;
}
